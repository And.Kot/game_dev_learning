![pipeline](http://gitlab.com/gitlab-org/gitlab-ce/badges/master/pipeline.svg?job=pipeline)

# game_dev_learning

Requirements:

- Linux
- CMake
- g++
- SDL2

Local testing: go to project repositore in your terminal and write two commands:

    - cmake -S . -B ./build
    - cmake --build ./build

