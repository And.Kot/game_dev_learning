cmake_minimum_required(VERSION 3.17)

project(dynamic)

add_executable(main_dynamic main.cpp)

find_package(SDL2 REQUIRED)

target_link_libraries(main_dynamic SDL2::SDL2 SDL2::SDL2main)

