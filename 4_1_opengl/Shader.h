#pragma once

#include <string>
#include <glew.h>

class __declspec(dllexport) Shader
{
public:
	Shader(const std::string& fileName);
	virtual ~Shader();

	void Bind();

private:
	static const unsigned int NUM_SHADERS = 2;

	GLuint program;
	GLuint shaders[NUM_SHADERS];
};