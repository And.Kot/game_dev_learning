#pragma once

#include <glm.hpp>
#include <glew.h>

class __declspec(dllexport) Vertex
{
public:
	Vertex(const glm::vec3& pos);
	virtual ~Vertex();

private:
	glm::vec3 pos;
};

class __declspec(dllexport) Mesh
{
public:
	Mesh(Vertex* vertices, unsigned int numVertices);
	virtual ~Mesh();

	void Draw();

private:
	enum
	{
		POSITION_VB,
		NUM_BUFFERS
	};

	GLuint vertexArrayObject;
	GLuint vertexArrayBuffers[NUM_BUFFERS];
	unsigned int drawCount;
};