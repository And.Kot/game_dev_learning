#ifndef MY_APP_H
#define MY_APP_H

#include <SDL2/SDL.h>
#include <defines.h>

class my_app {
public:
  my_app();
  virtual ~my_app();

  int OnExecute();

private:
  bool running;
  SDL_Window *window;
  SDL_Surface *surface;

  bool OnInit();
  void OnEvent(SDL_Event &);
  void OnLoop();
  void OnDraw();
  void OnQuit();
};

#endif // MY_APP_H
