#include <my_app.h>

bool my_app::OnInit() {
  if (SDL_Init(SDL_INIT_VIDEO) < 0)
    return false;

  window = SDL_CreateWindow(WINDOW_CAPTION, SDL_WINDOWPOS_UNDEFINED,
                            SDL_WINDOWPOS_UNDEFINED, WINDOW_WIDTH,
                            WINDOW_HEIGHT, SDL_WINDOW_SHOWN);
  if (window == nullptr)
    return false;

  surface = SDL_GetWindowSurface(window);
  if (surface == nullptr)
    return false;

  return true;
}
