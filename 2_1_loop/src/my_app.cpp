#include <my_app.h>

my_app::my_app() {
  window = nullptr;
  surface = nullptr;
}

my_app::~my_app() {}

int my_app::OnExecute() {
  if (!OnInit())
    return -1;

  // OnInit();
  SDL_Event e;
  running = true;
  while (running) {

    while (SDL_PollEvent(&e)) {
      OnEvent(e);
    }

    OnDraw();
    OnLoop();
  }

  OnQuit();

  return 0;
}
