#include <my_app.h>

void my_app::OnQuit() {
  SDL_FreeSurface(surface);
  surface = nullptr;

  SDL_DestroyWindow(window);
  window = nullptr;

  SDL_Quit();
}
