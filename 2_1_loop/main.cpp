#include <iostream>
#include <my_app.h>

int main() {
  std::cout << "It's 2_1_loop" << std::endl;

  my_app app;
  app.OnExecute();

  return 0;
}
